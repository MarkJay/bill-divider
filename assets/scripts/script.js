// this is the total bill
let totalBill = 0;

//this is the data from the name of friend and share inputs.
const shares = [];

//capture the span element
const billSpan = document.getElementById('totalBill');

//capture the input element
const totalBillInput = billSpan.parentElement.nextElementSibling;

//add an event listener
//on change event--- the value of the input MUST change, and you must remove the focus from the element
totalBillInput.addEventListener('change', function(){
	// get the value of totalBillInput and save it in our totalBill var.
	// REMEMBER--- input elements always return a string value.
	// We need to transform the data to the correct data type.
	// Float--- numbers with decimals, irrational numbers...
	// Int--- whole numbers
	totalBill = parseFloat(totalBillInput.value);
	//update the span to reflect the new value of total Bill
	billSpan.textContent = totalBill;
});

//This function is originally inside the addBtn event listener. But we decided to create a function so we can use it in the delete button
function publishShares(){
	let sumOfPersonalShare = 0;

	//to get the sum of the personal share, we need to loop through our shares array and add the share (indivShare) to the sumOfPersonalShare.
	shares.forEach(function(indivShare){
		sumOfPersonalShare += indivShare.share;
	});

	//To get the total group share
	const totalGroupShare = totalBill - sumOfPersonalShare;

	//But what we really want is the individual group share. To get the indiv group share, we'll divide the total group share by the number of friends in the shares array (we can get this via .length);
	//
	const indivGroupShare = totalGroupShare / shares.length;

	//our next goal is to add a new row containing the data we need to publish: name, personal share, group share and total.
	//
	//we need to loop through our shares array. (remember: each entry in the array should be pubslished in a table row.)
	
	//REVIEW!!!!! the second parameter of the anon function of an array loop method is the index of the individual data
	shares.forEach(function(indivShare, dataIndex){
		//we noticed in our html, we have a blank tbody. what we need to do now is to create the tr element and then insert it in our DOM (inside tbody);
		//create the tr element.
		const newRow = document.createElement('tr');

		//edit the inner html of newRow so we can publish the required data
		//${}
		newRow.innerHTML = `<td>${indivShare.name}</td>
		<td>${indivShare.share}</td>
		<td>${indivGroupShare}</td>
		<td>${ indivShare.share + indivGroupShare }</td>
		<td><button class="btn btn-danger deleteBtn" data-id="${dataIndex}">Remove</button></td>
		<td><button class="btn btn-success paidBtn">Paid</button></td>`

		//we need to insert our newly created table row in our dom by appending it to our tbody
		document.getElementById('shareDetails').appendChild(newRow);

		//At this point, we noticed that whenever we add a new friend, the old data remains. (As a result, we have duplicates)
		//So what we need to do, is to clear the tbody, whenever we add a new friend

	});
}

//capture add button
const addBtn = document.getElementById('friendShare').lastElementChild;

//add an event listener
addBtn.addEventListener('click', function(){

	document.getElementById('shareDetails').innerHTML = "";

	//first: capture the value of the name of friend.
	const nameOfFriend = addBtn.parentElement.firstElementChild.value;

	//second: capture the value of the share input'
	const shareInput = addBtn.previousElementSibling.value;

	//We need to save every button click in an array.
	//For us to be able to do that, we need to create an object that we will save in the shares array.
	const friendShare = {
		name: nameOfFriend,
		share: parseFloat(shareInput)
	};

	//push the created object in our shares array
	shares.push(friendShare);

	//Now, we need to get the value of the group share.
	// Groupshare = (TotalBill - (Sum of PersonalShare) ) / noOfFriends
	//We already have a value of the total bill.
	//Our next task is to get the value of the sum of the personal Share.
	
	//See the newly created function above
	//Instead of copying and pasting the whole process, we'll just create a function that we can call again
	//
	publishShares();
});

//At this point, we have a challenge. We want to attach an event listener to a dynamically created element.
//
//In order for us to know which element we clicked, we'll listen to the whole document.
//We'll capture the event. This is a parameter that we can receive in our anonymous function
document.addEventListener('click', function(event){
	// console.log(event);
	//We need to filter the action of this event listener if the element that triggered the event has a class of editBtn
	if(event.target.classList.contains('paidBtn')){
		//if we clicked the right element, we need to change the background of the tr of that element into green (bg-success);
		//we just need to add the class to the element.
		//the Paid button is represented by event.target
		event.target.parentElement.parentElement.classList.add('bg-success');
	}

	//We need to filter the elements and only do this action if the element has a class of deleteBtn;
	if(event.target.classList.contains('deleteBtn')){
		document.getElementById('shareDetails').innerHTML = "";
		//We need to delete the data from the shares array.
		//To delete the data, we need the index of that data from the delete button.
		//To get the index, we need to get the value of the data-id attribute we added in our newRow-td-button
		//REMEMBER: In this instance, event.target represents the delete button;
		
		const index = event.target.getAttribute('data-id');

		//delete the data from the shares array;
		shares.splice(index, 1);

		//When we delete a data from our shares array, we are also deleting the row. And when we do that, we will also recompute the sumOfPersonalShares, totalGroupShares, indivGroupShare
		//We will recreate the innerHTML of the tbody.
		publishShares();
	}
})